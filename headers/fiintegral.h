#ifndef FIINTEGRAL_H
#define FIINTEGRAL_H

double count_p_integral(double n, double k1, double k2, double p, double q);
double fi_big(double x);
double count_x_integral(double n, double k, double p, double q);

#endif
