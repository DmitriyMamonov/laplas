all: laplas

laplas: ./src/main.c fiintegral.o filocal.o
	gcc -g -Wall  fiintegral.o filocal.o ./src/main.c -lm -o laplas

fiintegral.o: ./src/fiintegral.c
	gcc -g -Wall -lm -c ./src/fiintegral.c

filocal.o: ./src/filocal.c
	gcc -g -Wall -lm -c ./src/filocal.c

install:
	rm fiintegral.o filocal.o
