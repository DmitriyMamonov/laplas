#include <stdio.h>
#include <stdlib.h>
#include "../headers/filocal.h" //Заголовочник для локальной формулы
#include "../headers/fiintegral.h" //Заголовочник для интегральной формулы

int main( void )
{
	//Количество повторений
	unsigned int n;
	//Количество исходных повторений(для локальной формулы)
	unsigned int k;
	//Минимальное количество исходных повторениний(для интегральной формулы)
	unsigned int k1;
	//Максимальное количество исходных повторениний(для интегральной формулы)
	unsigned int k2;
	//Вероятность исходного события
	double p;
	char f;
	puts("Программа для вычисления вероятности повторений по Лапласу");

	printf("%s", "Введите l, ecли хотите посчитать по локальной формуле Лапласа и i, если по интегральной: ");
	scanf("%c", &f); //считываем определитель формулы

	printf("%s", "Введите n: ");
	scanf("%d", &n); //считываем кол-во повторений

	if( f == 'l')
	{
		printf("%s", "Введите k: ");
		scanf("%d", &k); //считываем k

		printf("%s", "Введите p: ");
		scanf("%le", &p);

		double x = count_x(n, k, p, 1.0 - p); //посчитали значение аргумента x
		double var_fi = fi(x); //посчитали значение функции fi(x)
		double p_n = count_p(n, k, p, 1.0 - p); //выполняются слишком много действий внутри!

		printf("x = %.4f\n", x);
		printf("fi(x) = %.4f\n", var_fi);
		printf("Искомая p(n,k) = %f\n", p_n);

	}
	else if ( f == 'i' )
	{
		printf("%s", "Введите k1: ");
		scanf("%d", &k1); //считываем k1
		printf("%s", "Введите k2: ");
		scanf("%d", &k2);

		printf("%s", "Введите p: ");
		scanf("%le", &p);

		double x1 = count_x_integral(n, k1, p, 1.0 - p); //посчитали значение аргумента x1
		double x2 = count_x_integral(n, k2, p, 1.0 - p); //посчитали значение аргумента x2
		double var_fi1 = fi_big(x1); //посчитали значение функции fi1(x)
		double var_fi2 = fi_big(x2); //посчитали значение функции fi2(x)
		double p_n = count_p_integral(n, k1, k2, p, 1.0 - p); //выполняются слишком много действий внутри!

		printf("x1 = %.4f\n", x1);
		printf("x2 = %.4f\n", x2);
		printf("FI_1(x) = %.4f\n", var_fi1);
		printf("FI_2(x) = %.4f\n", var_fi2);
		printf("Искомая p(n,k) = %.4f\n", p_n);

	}
	else 
	{
		puts("Неправильный идентификатор!!");
		exit(1);
	}

	return 0;

}
