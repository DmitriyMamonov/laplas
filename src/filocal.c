#include <math.h>
#include "../headers/filocal.h"
#define EXP exp(1)
#define PI 3.1416

double count_p(double n, double k, double p, double q)
{
	double x = count_x(n, k, p, q);
	return 1.0 / sqrt(n * p * q) * fi(x);
}

double fi(double x)
{
	return (1.0 / sqrt(2.0 * PI)) * pow(EXP, -(x * x) / 2.0);
}

double count_x(double n, double k, double p, double q)
{
	return (k - n * p) / sqrt(n * p * q);
}
