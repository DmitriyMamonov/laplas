#include <math.h>
#include "../headers/fiintegral.h"
#define PI 3.14159
#define EXP exp(1)

double count_p_integral(double n, double k1, double k2, double p, double q)
{
	double x1 = count_x_integral(n, k1, p, q);
	double x2 = count_x_integral(n, k2, p, q);
	return fi_big(x2) - fi_big(x1);
}

double fi_big(double x)
{
	return 1 / sqrt(2 * PI) * ((sqrt(PI) * erf(x / sqrt(2.0)) / sqrt(2.0)) - (sqrt(PI) * erf(0 / sqrt(2.0)) / sqrt(2.0)));
}

double count_x_integral(double n, double k, double p, double q)
{
	return (k - n * p) / sqrt(n * p * q);
}
